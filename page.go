package page

import (
	"crypto/tls"
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
	"time"

	"gitlab.com/billy.harvey/lib"

	"github.com/mmcdole/gofeed"
)

type Page struct {
	Body     string
	Link     string
	Final    string
	Language string
	Etag     string
	Status   int
	Mimes    []string
	Mime     string
	Error    error
	Feed     *gofeed.Feed
}

func (p *Page) Feeds() {
	p.Feed, p.Error = gofeed.NewParser().ParseString(p.Body)
	if p.Error == nil {
		p.Feed.Title = strings.TrimSpace(p.Feed.Title)
		if p.Feed.Language != "" {
			p.Language = p.Feed.Language
		}
	}
}

func New(link string, etag string) *Page {
	return &Page{Link: link, Etag: etag}
}

func (p *Page) Get() {
	p.Link = strings.TrimSpace(p.Link)
	if p.Link == "" {
		return
	}
	if !strings.HasPrefix(p.Link, "http") {
		p.Link = "http://" + p.Link
	}
	if len(p.Mimes) > 0 {
		res := p.Fetch("HEAD")
		if p.Error != nil {
			if strings.HasSuffix(p.Link, "/") {
				p.Link = p.Link[:len(p.Link)-1]
			} else {
				p.Link = p.Link + "/"
			}
			res = p.Fetch("HEAD")
			if p.Error != nil {
				return
			}
		}
		if !lib.StringSliceHasPrefix(p.Mimes, p.Mime) {
			res.Body.Close()
			p.Error = errors.New("mime mismatch")
			return
		}
	}
	res := p.Fetch("GET")
	if p.Error != nil {
		if strings.HasSuffix(p.Link, "/") {
			p.Link = p.Link[:len(p.Link)-1]
		} else {
			p.Link = p.Link + "/"
		}
		res = p.Fetch("GET")
		if p.Error != nil {
			return
		}
	}
	var body []byte
	body, p.Error = ioutil.ReadAll(res.Body)
	if p.Error == nil {
		p.Body = string(body)
	}
	res.Body.Close()
	p.Link = lib.UnHttp(p.Link)
	p.Final = lib.UnHttp(p.Final)
}

func (p *Page) Fetch(method string) *http.Response {
	var req *http.Request
	var res *http.Response
	transport := &http.Transport{
		Dial:                  (&net.Dialer{Timeout: time.Duration(10) * time.Second}).Dial,
		DisableKeepAlives:     true,
		ExpectContinueTimeout: time.Duration(10) * time.Second,
		ResponseHeaderTimeout: time.Duration(10) * time.Second,
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		TLSHandshakeTimeout:   time.Duration(10) * time.Second}
	client := &http.Client{Timeout: time.Duration(10) * time.Second, Transport: transport}
	req, p.Error = http.NewRequest(method, p.Link, nil)
	if p.Error == nil {
		req.Header.Set("User-Agent", "Googlebot-News")
		req.Header.Set("Referer", "http://www.google.com")
		req.Header.Set("If-None-Match", p.Etag)
		res, p.Error = client.Do(req)
		transport.CloseIdleConnections()
		if p.Error == nil {
			for key := range res.Header {
				switch strings.ToLower(key) {
				case "etag":
					p.Etag = strings.TrimSpace(res.Header[key][0])
				case "content-type":
					p.Mime = strings.TrimSpace(strings.Split(res.Header[key][0], ";")[0])
				}
			}
			p.Status = res.StatusCode
			p.Final = res.Request.URL.String()
		}
	}
	return res
}
